var catalogue = [];

catalogue[0] = {
    titre: "Mon_titre", description: "ma_descript", responsable: "Pink Floyd", enseignants: ["Dark side of the moon", "Wish you were here"]
};
catalogue[1] = {
    titre: "Mon_titre", description: "ma_descript", responsable: "Jimi Hendrix", enseignants: ["Electric Landyland", "Band of Gypsys"]
};

var express = require('express');
var server = express();

server.set('view engine', 'ejs');


server.get('/:num', function (req, res) {
    //if (isNaN(parseInt(req.params.num, 10))) {
    if (req.params.num < 0 || req.params.num > catalogue.length -1) {
        console.log("Error get num");
        res.send("Error !");
    }
    else
        res.render('cours.ejs', catalogue[req.params.num]);
});
server.listen(8080);