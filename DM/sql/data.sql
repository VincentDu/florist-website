DROP DATABASE IF EXISTS `data`;

CREATE DATABASE IF NOT EXISTS `data` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `data`;

drop table if exists personnes cascade;
drop table if exists clients cascade;
drop table if exists employes cascade;
drop table if exists fleurs cascade;

/*email text NOT NULL,*/
create table personnes(
nom text NOT NULL, 
prenom text NOT NULL,
pwd text NOT NULL,
email VARCHAR(100),
admin boolean NOT NULL DEFAULT FALSE,
PRIMARY KEY(email)
);

create table clients(
cid serial,
email VARCHAR(100),
PRIMARY KEY(cid),
FOREIGN KEY (email) REFERENCES personnes(email)
);

/*ALTER TABLE clients
DROP CONSTRAINT clients_email_fkey,
ADD FOREIGN KEY (email) REFERENCES personnes(email) ON DELETE CASCADE;*/


create table employes(
eid serial,
email VARCHAR(100),
PRIMARY KEY(eid),
FOREIGN KEY (email) REFERENCES personnes(email)
);

/*ALTER TABLE employes
DROP CONSTRAINT employes_email_fkey,
ADD FOREIGN KEY (email) REFERENCES personnes(email) ON DELETE CASCADE;*/

INSERT INTO personnes(nom, prenom, pwd, email) VALUES 
('Dupont','Chris','mdp1','a@b'),
('Mad','Marin','mdp1','aa@b'),
('Auto','Damien','mdp1','aaa@b'),
('Pumas','Charlenne','mdp1','aaaa@b'),
('Rose','Derick','mdp1','aaaaa@b'),
('Springboks','Antoine','mdp1','aaaaaa@b');

INSERT INTO clients VALUES 
(1,'a@b'),
(2,'aa@b'),
(3,'aaa@b');


INSERT INTO employes VALUES 
(1,'aaaa@b'),
(2,'aaaaa@b'),
(3,'aaaaaa@b');

UPDATE personnes SET admin=TRUE WHERE email IN (SELECT email FROM employes);

create table fleurs(
fid serial,
nom text NOT NULL,
prix FLOAT,
PRIMARY KEY(fid)
);

INSERT INTO fleurs(nom, prix) VALUES 
('Rose',3),
('Bleuet',5),
('Orchidée',11.99),
('Anémone',11.50),
('Capucine',10.30);


create table bouquets(
bid serial,
fid integer,
quantite integer,
FOREIGN KEY (fid) REFERENCES fleurs(fid)
);

INSERT INTO bouquets(bid,fid, quantite) VALUES 
(1,1,3),
(1,2,3),
(1,3,2),
(2,3,3),
(2,2,3),
(2,1,2),
(3,3,3),
(3,2,3),
(3,1,2),
(4,3,5),
(4,2,2),
(4,1,6),
(5,3,1),
(5,2,2),
(5,1,3);