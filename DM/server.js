/*var catalogue = [];

catalogue[0] = {
    titre: "Mon_titre", description: "ma_descript", responsable: "Pink Floyd", enseignants: ["Dark side of the moon", "Wish you were here"]
};
catalogue[1] = {
    titre: "Mon_titre", description: "ma_descript", responsable: "Jimi Hendrix", enseignants: ["Electric Landyland", "Band of Gypsys"]
};
*/
var express = require('express');
var server = express();
var router = express.Router();
var mysql = require('mysql');
var session = require('express-session');


server.use(express.urlencoded({extended: true}));
server.use(express.static('images'));
server.use(express.static('views/css'));
server.use(express.static('sql'));
server.set('view engine', 'ejs');

var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'vincent@localhost',
	password : '',
	database : 'data'
});

server.post('/auth', function(req, res) {
	var email = req.body.email;
	var password = req.body.password;
	if (email && password) {
		connection.query('SELECT * FROM personnes WHERE email = ? AND pwd = ?', [email, password], function(error, results, fields) {
			/*if (error) {
				res.send('Wrong email or password!');
			}*/
			if (results.length > 0) {
			//else {
				req.session.loggedin = true;
				req.session.email = email;
				//mettre le bon role
                //req.session.role;
				//{ title: 'User List', Data: results}
				res.redirect('/home');
			/*} else {
				res.send('Wrong email or password!');
			}*/
			}
			res.end();
		});
	} else {
		res.send('Please enter email and Password!');
		res.end();
	}
});

var user = {titre : "Veuillez remplir ce formulaire", nom: undefined , mdp :undefined};

/*server.get('/:num', function (req, res) {
    //if (isNaN(parseInt(req.params.num, 10))) {
    if (req.params.num < 0 || req.params.num > catalogue.length -1) {
        console.log("Error get num");
        res.send("Error !");
    }
    else
        res.render('cours.ejs', catalogue[req.params.num]);
});*/

server.get("/login", function (req, res) {
    res.render('form.ejs', user);
});

server.get("/", function (req, res) {
    res.render('home.ejs', user);
});

server.post("/auth", function (req, res) {
    res.render('home.ejs', user);
});

server.listen(8080);